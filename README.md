# Typescript - Course

www.udemy.com/understanding-typescript

### Instructor

[Maximilian Schwarzmüller]

#### Run

```npm install```

* To compile .ts files into .js: 

```sudo npm -g install typescript```
```tsc 'file-name.ts'```

#### Initialice a live server

```npm start```


[Maximilian Schwarzmüller]: <https://www.udemy.com/user/maximilian-schwarzmuller/>

